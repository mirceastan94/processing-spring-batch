package com.batch.demo.config.writer;

import com.batch.demo.entity.Customer;
import com.batch.demo.utils.CustomerPreparedStatementConverter;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

/**
 * This class defines the customer item writer implementation
 */
public class CustomerItemWriter implements ItemWriter<Customer> {

    private DataSource dataSource;

    private NamedParameterJdbcTemplate jdbcTemplate;

    public CustomerItemWriter(DataSource dataSource, NamedParameterJdbcTemplate jdbcTemplate) {
        this.dataSource = dataSource;
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String QUERY_INSERT_TASK = "insert into customer (id, name, join_date, transactions) values (?, ?, ?, ?)";

    @Override
    public void write(List<? extends Customer> customers) throws Exception {
        customerItemWriter(dataSource, jdbcTemplate).write(customers);
    }

    private JdbcBatchItemWriter<Customer> customerItemWriter(DataSource dataSource, NamedParameterJdbcTemplate jdbcTemplate) {
        JdbcBatchItemWriter<Customer> customerItemWriter = new JdbcBatchItemWriter<>();
        customerItemWriter.setDataSource(dataSource);
        customerItemWriter.setJdbcTemplate(jdbcTemplate);
        customerItemWriter.setSql(QUERY_INSERT_TASK);

        ItemPreparedStatementSetter<Customer> customerMapper = new CustomerPreparedStatementConverter();
        customerItemWriter.setItemPreparedStatementSetter(customerMapper);

        return customerItemWriter;
    }

}
