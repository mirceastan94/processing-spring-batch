package com.batch.demo.config.processor;

import com.batch.demo.entity.Customer;
import org.springframework.batch.item.validator.ValidatingItemProcessor;
import org.springframework.batch.item.validator.ValidationException;

/**
 * This class defines the processor for filtering customers based on their transactions number
 */
public class TransactionValidatingProcessor extends ValidatingItemProcessor<Customer> {

    public TransactionValidatingProcessor(final int limit) {
        super(item -> {
            if (item.getTransactions() >= limit) {
                throw new ValidationException("Customer has more than " + limit + " transactions!");
            }
        });
        setFilter(true);
    }

}
