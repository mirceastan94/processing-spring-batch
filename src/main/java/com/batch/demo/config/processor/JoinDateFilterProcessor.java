package com.batch.demo.config.processor;

import com.batch.demo.entity.Customer;
import org.springframework.batch.item.ItemProcessor;

import java.time.LocalDate;

/**
 * This class defines the processor for filtering customers based on their join date
 */
public class JoinDateFilterProcessor implements ItemProcessor<Customer, Customer> {

    @Override
    public Customer process(final Customer customer) {
        if (LocalDate.now().getMonth() == customer.getJoinDate().getMonth()) {
            return customer;
        }
        return null;
    }
}
