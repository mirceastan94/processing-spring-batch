package com.batch.demo.config;

import com.batch.demo.config.processor.JoinDateFilterProcessor;
import com.batch.demo.config.processor.TransactionValidatingProcessor;
import com.batch.demo.config.reader.CustomerItemReader;
import com.batch.demo.config.writer.CustomerItemWriter;
import com.batch.demo.entity.Customer;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.util.Arrays;

/**
 * This class defines quartz customer report job configuration
 */
@Configuration
public class ReportJobConfig {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @StepScope
    @Bean
    public ItemReader<Customer> itemReader() {
        return new CustomerItemReader();
    }

    @StepScope
    @Bean
    public ItemProcessor<Customer, Customer> itemProcessor() {
        final CompositeItemProcessor<Customer, Customer> customerProcessor = new CompositeItemProcessor<>();
        customerProcessor.setDelegates(Arrays.asList(new JoinDateFilterProcessor(), new TransactionValidatingProcessor(5)));
        return customerProcessor;
    }

    @StepScope
    @Bean
    public ItemWriter<Customer> itemWriter() {
        return new CustomerItemWriter(dataSource, jdbcTemplate);
    }

    @Bean
    public Job customerReportJob() {
        return jobBuilderFactory.get("customerReportJob").incrementer(new RunIdIncrementer()).start(taskletStep()).next(chunkStep()).build();
    }

    @Bean
    public Step chunkStep() {
        return stepBuilderFactory.get("chunkStep").<Customer, Customer>chunk(3)
                .reader(itemReader()).processor(itemProcessor()).writer(itemWriter()).build();
    }

    @Bean
    public Step taskletStep() {
        return stepBuilderFactory.get("taskletStep").tasklet(tasklet()).build();
    }

    @Bean
    public Tasklet tasklet() {
        return (stepContribution, chunkContext) -> RepeatStatus.FINISHED;
    }

}
