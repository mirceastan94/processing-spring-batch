package com.batch.demo.config.reader;

import com.batch.demo.entity.Customer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * This class defines the customer item reader implementation
 */
public class CustomerItemReader implements ItemReader<Customer> {

    private int nextCustomerIndex;
    private List<Customer> customers;

    public CustomerItemReader() {
        initializeCustomers();
    }

    @Override
    public Customer read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        Customer nextCustomer = null;
        if (nextCustomerIndex < customers.size()) {
            nextCustomer = customers.get(nextCustomerIndex);
            nextCustomerIndex++;
        }
        return nextCustomer;
    }

    private void initializeCustomers() {
        final Customer firstCustomer = new Customer(UUID.randomUUID(), "Tony", LocalDate.of(1985, 2, 2), 6);
        final Customer secondCustomer = new Customer(UUID.randomUUID(), "Nick", LocalDate.of(1992, 2, 12), 3);
        final Customer thirdCustomer = new Customer(UUID.randomUUID(), "Ian", LocalDate.of(1994, 9, 20), 1);
        customers = Arrays.asList(firstCustomer, secondCustomer, thirdCustomer);
        nextCustomerIndex = 0;
    }
}
