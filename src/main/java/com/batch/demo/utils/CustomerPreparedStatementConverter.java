package com.batch.demo.utils;

import com.batch.demo.entity.Customer;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * This class defines the customer prepared statement converter
 */
public class CustomerPreparedStatementConverter implements ItemPreparedStatementSetter<Customer> {

    @Override
    public void setValues(Customer customer, PreparedStatement ps) throws SQLException {

        // ID
        ps.setObject(1, customer.getId());

        // Name
        ps.setString(2, customer.getName());

        // Join date
        ps.setTimestamp(3, java.sql.Timestamp.valueOf(customer.getJoinDate().atStartOfDay()));

        // Transactions
        ps.setInt(4, customer.getTransactions());
    }
}

