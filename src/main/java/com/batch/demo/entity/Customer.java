package com.batch.demo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

/**
 * This class defines the customer entity
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
public class Customer {

    public Customer(UUID id, String name, LocalDate joinDate, Integer transactions) {
        this.id = id;
        this.name = name;
        this.joinDate = joinDate;
        this.transactions = transactions;
    }

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "uuid")
    @Column(unique = true, nullable = false, columnDefinition = "uuid")
    @Type(type = "pg-uuid")
    private UUID id;

    @Column
    private String name;

    @Column
    private LocalDate joinDate;

    @Column
    private Integer transactions;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Customer customer = (Customer) obj;
        return Objects.equals(id, customer.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
